import React from "react";
import "./App.css";
import CallWaiting from "./components/contents/call_waiting";
import OngoingCall from "./components/contents/ongoing_call";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <CallWaiting />
        </Route>
        <Route exact path="/call">
          <OngoingCall />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
