import React, { Component } from "react";
import Logo from "../../images/Logo/Full.png";
import AppBar from "@material-ui/core/AppBar";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    logoImage: {
      height: "53px",
      width: "232px",
      marginLeft: "auto",
      marginRight: "auto",
      marginBottom: "3px",
      marginTop: "3px"
    }
  });

export class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <AppBar color="white">
          <img src={Logo} alt="logo" height="53px" width="232px" className={classes.logoImage}/>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(Header);
