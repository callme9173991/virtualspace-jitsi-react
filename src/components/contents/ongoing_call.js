import React, { Component, Fragment } from "react";
import "./index.scss";
import WebCamDisabled from "../../images/Webcam Disabled.png";
import Avatar from "../../images/Graphic/Default Avatar Personal Circle.png";
import TurnedOffCam from "../../images/Label/Icon Camera Off Active.png";
import MutedMic from "../../images/Label/Icon Mic Off Active.png";
import SettingIcon from "../../images/Label/Icon Setting.png";
import InfoIcon from "../../images/Label/Icon Info.png";
import LinkIcon from "../../images/Label/Icon Copy Link.png";
import HelpIcon from "../../images/Label/Icon Help.png";
import LeaveIcon from "../../images/Label/Icon Leave Call.png";
import { Popover, OverlayTrigger, Button, Modal, Form } from "react-bootstrap";
import UserAvatar from "../../images/Graphic/Default Avatar Personal.png";
import IconCamera from "../../images/Label/Icon Camera.png";
import IconMicOff from "../../images/Label/Icon Mic Off.png";
import Woman from "../../images/woman.png";
import Man from "../../images/man.png";
import IconCameraOn from "../../images/Label/Icon Camera Off.png";
import IconMicOn from "../../images/Mic off.png";

export class OngoingCall extends Component {
  constructor(props) {
    super(props);

    this.state = {
      turnOnCam: false,
      show: false,
      CamStatus: true,
      MicStatus: true,
    };
  }

  turnOnCam = () => {
    this.setState({
      turnOnCam: !this.state.turnOnCam
    });
  };

  handleHide = () => {
    this.setState({
      show: !this.state.show
    });
  };

  showMoal = () => {
    this.setState({
      show: !this.state.show
    });
  };

  handleCamera = () => {
      this.setState({
          CamStatus: !this.state.CamStatus
      })
  }

  handleMic = () => {
    this.setState({
        MicStatus: !this.state.MicStatus
    })
  }

  handleLeave = () => {
      window.location.href = "/";
  }

  render() {
    const popover = (
      <Popover id="popover-basic">
        <Popover.Title as="h3">Participants</Popover.Title>
        <Popover.Content>
          <div className="row">
            <div className="col-sm-12 pop_over">
              <img src={UserAvatar} alt="UserAvatar" width="30px" />
              <span className="text">Dr. Marcell</span>
              <span className="image">
                <img src={IconCamera} alt="Icon Camera Off" width="18px" />
                <img src={IconMicOff} alt="Icon Mic Off" width="18px" />
              </span>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-sm-12 pop_over">
              <img src={UserAvatar} alt="UserAvatar" width="30px" />
              <span className="text">Dr. Marcell</span>
              <span className="image">
                <img src={IconCamera} alt="Icon Camera Off" width="18px" />
                <img src={IconMicOff} alt="Icon Mic Off" width="18px" />
              </span>
            </div>
          </div>
        </Popover.Content>
      </Popover>
    );
    return (
      <Fragment>
        <div className="container ongoing_back" style={{ padding: "0px" }}>
          <div className="webCamera">
            {!this.state.turnOnCam ? (
              <Fragment>
                <img src={WebCamDisabled} alt="webcamdisabled" width="20px" />
                <p>Your webcam isn't enable yet</p>
                <button className="btn" onClick={this.turnOnCam}>
                  Turn on webcam
                </button>
              </Fragment>
            ) : (
              <img
                src={Avatar}
                alt="webCam"
                width="100%"
                style={{ marginTop: "0px" }}
              />
            )}
          </div>

          {!this.state.turnOnCam ? (
            <div className="avatar_div">
              <img src={Avatar} alt="avatar" width="100px" className="avatar" />
              <h6>Nicholas Ng</h6>
              <p>Turn off his camera</p>
            </div>
          ) : (
            <img src={Woman} alt="Reciever" width="100%" style={{height:"-webkit-fill-available" }} />
          )}
        </div>

        <footer>
          <div className="blank">.</div>
          <div className="icon_div turn_off" style={{ borderLeft: "1px solid grey" }} onChange={this.handleCamera} >
            <img
              src={!this.state.turnOnCam && this.state.CamStatus ? TurnedOffCam : IconCameraOn}
              alt="TurnedOffCam"
              width="20px"
            />
            <p>Turn off</p>
          </div>
          <div className="icon_div mute_mic" onChange={this.handleMic}>
            <img
              src={!this.state.turnOnCam && this.state.MicStatus ? MutedMic : IconMicOn}
              alt="MutedMic"
              width="20px"
            />
            <p>Mute Mic</p>
          </div>
          <div
            className="icon_div settings"
            style={{ cursor: "pointer" }}
            onClick={this.handleHide}
          >
            <img src={SettingIcon} alt="SettingIcon" width="20px" />
            <p>Settings</p>
          </div>
          <div className="info">
            <img src={InfoIcon} alt="InfoIcon" width="20px" />
          </div>

          <OverlayTrigger trigger="click" placement="top" overlay={popover}>
            <div className="info_text">
              <span>In call with: Nicholas Ng</span> <br />
              Call duration: 12:05
            </div>
          </OverlayTrigger>

          <div className="right_blank">.</div>
          <div
            className="right_icon_div leave"
            style={{ borderRight: "1px solid grey", cursor: "pointer" }}
            onClick={this.handleLeave}
          >
            <img src={LeaveIcon} alt="LeaveIcon" width="20px" />
            <p>Leave call</p>
          </div>
          <div className="right_icon_div get_help">
            <img src={HelpIcon} alt="HelpIcon" width="20px" />
            <p>Get help</p>
          </div>
          <div className="link_button">
            <button className="btn">
              <img src={LinkIcon} alt="LinkIcon" width="20px" /> Copy Invitation
            </button>
          </div>
          <div className="url_text">
            <a href="#">Room: https://virtualspace.ai/v/12421841</a>
          </div>
        </footer>
        <Modal
          show={this.state.show}
          size="md"
          aria-labelledby="contained-modal-title-vcenter"
          closeButton={false}
          onHide={this.showMoal}
        >
          <Modal.Header closeButton>Video Call Settings</Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-sm-1"></div>
              <div className="col-sm-10" style={{ textAlign: "center" }}>
                <img
                  src={Man}
                  alt="picture"
                  width="100%"
                  style={{ padding: "5% 10%" }}
                />
                <Form>
                  <Form.Group controlId="selectCamera">
                    <Form.Label style={{ float: "left" }}>Camera</Form.Label>
                    <Form.Control as="select">
                      <option>Default-Face Time HD Camera</option>
                      <option>Best-Face Time HD Camera</option>
                    </Form.Control>
                  </Form.Group>
                  <Form.Group controlId="selectMic">
                    <Form.Label style={{ float: "left" }}>
                      Microphone
                    </Form.Label>
                    <Form.Control as="select">
                      <option>Default-Macbook Pro Microphone(Built-in)</option>
                      <option>Best- Microphone</option>
                    </Form.Control>
                  </Form.Group>
                  <Form.Group controlId="selectSpeaker">
                    <Form.Label style={{ float: "left" }}>Speaker</Form.Label>
                    <Form.Control as="select">
                      <option>Default-Macbook Pro Speakers(Built-in)</option>
                      <option>Best-Macbook Pro Speaker</option>
                    </Form.Control>
                  </Form.Group>
                </Form>
                <button
                  className="btn"
                  style={{
                    backgroundColor: "#0074d2",
                    color: "white",
                    float: "right"
                  }}
                  onClick={this.handleHide}
                >
                  Save Changes
                </button>
                &nbsp;
                <button
                  className="btn btn-default"
                  style={{
                    color: "#0074d2",
                    border: "1px solid grey",
                    float: "right",
                    marginRight: "10px"
                  }}
                  onClick={this.handleHide}
                >
                  Cancel
                </button>
                &nbsp;
              </div>
              <div className="col-sm-1"></div>
            </div>
          </Modal.Body>
        </Modal>
      </Fragment>
    );
  }
}

export default OngoingCall;
