import React, { Component } from "react";
import { Card, CardMedia, CardContent } from "@material-ui/core";
import Header from "../layout/header";
import Footer from "../layout/footer";
import "./index.scss";
import Man from "../../images/man.png";
import Woman from "../../images/woman.png";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import LinkImage from "../../images/Label/Icon Copy Link.png";
import CameraImage from "../../images/Label/Icon Start Video.png";
import CameraOff from "../../images/camera.png";
import MicOff from "../../images/mic.png";
import { Modal } from "react-bootstrap";
import BlockCamera from "../../images/Camera Blocked.png";
import WebCamDisabled from "../../images/Webcam Disabled.png";
import Info from "../../images/Label/Icon Info.png";
import MicOffed from "../../images/Mic off.png";
import CameraOffed from "../../images/Camera Off.png";
import BrowserNotSupported from "../../images/Browser Not Supported.png"; 


const styles = theme => ({
  image: {
    width: "100%",
    padding: "10%"
  },
  content: {
    marginTop: "10%",
    marginBottom: "10%"
  },
  btn: {
    textAlign: "center"
  },
  modalHeader: {
    backgroundColor: "#0074d2",
    height: "120px"
  }
});

export class CallWaiting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      isBrowserSupported: true
    };
  }

  handleHide = () => {
    this.setState({
      show: !this.state.show
    });
    window.location.href = "/call";
  };
  showMoal = () => {
    this.setState({
      show: !this.state.show
    });
  };

  render() {
    const { classes } = this.props;
    const { isBrowserSupported } = this.state;
    return (
      <div className="container">
        <Header />
        <Card className="card_contents">
          <CardMedia>
            <div className="row">
              {isBrowserSupported ? (
                <div
                  className="col-md-6"
                  style={{ position: "relative", textAlign: "center" }}
                >
                  <img src={Man} alt="man" className={classes.image} />
                  <div
                    style={{
                      position: "absolute",
                      top: "75%",
                      left: "50%",
                      transform: "translate(-50%, -50%)"
                    }}
                  >
                    <img src={MicOff} alt="micoff" width="55px" /> &nbsp;&nbsp;
                    <img src={CameraOff} alt="cameraoff" width="55px" />
                  </div>
                </div>
              ) : (
                <div
                  className="col-md-6"
                  style={{ position: "relative", textAlign: "center" }}
                >
                <img src={BrowserNotSupported} alt="man" className={classes.image} />
                {/*    <div 
                //     style={{
                //       margin: "10%",
                //       backgroundImage: "linear-gradient(#989898, #4d4d4d)"
                //       //   height: "72%"
                //     }}
                //   >
                //     <img
                //       src={Info}
                //       alt="info"
                //       width="50px"
                //       style={{ marginTop: "8%" }}
                //     />
                //     <br />
                //     <br />
                //     <h5>Browser not supported!</h5>
                //     <p>
                //       Please use Google Chrome or Firefox so we can access your
                //       camera and microphone.
                //     </p>
                //     <img
                //       src={MicOffed}
                //       alt="MicOffed"
                //       width="25px"
                //       style={{ marginRight: "40px", marginBottom: "50px" }}
                //     />
                //     <img
                //       src={CameraOffed}
                //       alt="CameraOffed"
                //       width="25px"
                //       style={{ marginBottom: "50px" }}
                //     />
                //   </div>*/}
                </div>
              )}
              <div className="col-md-6">
                <img src={Woman} alt="woman" className={classes.image} />
              </div>
            </div>
          </CardMedia>
          <CardContent>
            <div className="row">
              <div className="col-md-4 col-sm-1"></div>
              <div className="col-md-4 col-sm-10" style={{textAlign: "center"}}>
                <Typography gutterBottom variant="h5" component="h2">
                  Ready to join?
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Time: Mar 20, 2020 08:00 PX Asia/Kuala Lumpur Meeting
                  Password: 1902390
                </Typography>
              </div>
              <div className="col-md-4 col-sm-1"></div>
            </div>
          </CardContent>

          <div className="row" style={{ marginBottom: "50px" }}>
            <div className="col-md-4 col-sm-1"></div>
            <div className="col-md-4 col-sm-10">
              <div className="row">
                <div className="col-sm-6">
                  <button
                    className="btn"
                    size="small"
                    style={{ width: "100%", backgroundColor: "#737a83" }}
                  >
                    <img src={LinkImage} alt="linkImage" width="20px" />
                    &nbsp;
                    <span style={{ color: "white" }}>Copy Invitation</span>
                  </button>
                </div>
                <div className="col-sm-6">
                  <button
                    className="btn"
                    size="small"
                    onClick={this.showMoal}
                    style={{ width: "100%", backgroundColor: "#0074d2" }}
                  >
                    <img src={CameraImage} alt="cameraImage" width="20px" />
                    &nbsp;<span style={{ color: "white" }}>Start Meeting</span>
                  </button>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-1"></div>
          </div>
        </Card>
        <Modal
          show={this.state.show}
          size="md"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          closeButton={false}
          onHide={this.showMoal}
        >
          <Modal.Header closeButton className={classes.modalHeader}>
            <img
              src={CameraOff}
              alt="camera"
              width="55px"
              style={{
                marginLeft: "auto",
                marginTop: "5%",
                verticalAlign: "middle"
              }}
            />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <img
              src={MicOff}
              alt="MicOff"
              width="55px"
              style={{
                float: "left",
                marginTop: "5%",
                verticalAlign: "middle"
              }}
            />
          </Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-sm-1"></div>
              <div className="col-sm-10" style={{ textAlign: "center" }}>
                <h4>Your camera and microphone are blocked</h4>
                <p>
                  Allow access to your cam/mic by clicking on blocked cam icon
                  in your browser address bar.{" "}
                  <span>
                    <img src={BlockCamera} alt="blockCamera" width="23px" />
                  </span>
                </p>
                <button
                  className="btn"
                  style={{
                    backgroundColor: "#0074d2",
                    color: "white",
                    width: "95%"
                  }}
                  onClick={this.handleHide}
                >
                  <img src={WebCamDisabled} alt="camera" width="20px" />
                  &nbsp;&nbsp; Continue without Camera & Mic
                </button>
              </div>
              <div className="col-sm-1"></div>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default withStyles(styles)(CallWaiting);
